var gulp        = require('gulp');
var config      = require('../config.json');
var browsersync = require('browser-sync').create();

gulp.task('serve', ['build'], () => {

  browsersync.init({
    server: `./${config.paths.dist}`
  });

  gulp.watch(`${config.paths.src}/**/**.js`, ['build']);
  gulp.watch(`${config.paths.src}/**/**.css`, ['build']);
  gulp.watch(`${config.paths.src}/**/**.tag`, ['build']);
  gulp.watch(`${config.paths.src}/**/**.html`, ['build']);

  gulp.watch(`${config.paths.dist}/**/**.js`).on('change', browsersync.reload);
  gulp.watch(`${config.paths.dist}/**/**.tag`).on('change', browsersync.reload);
  gulp.watch(`${config.paths.dist}/**/**.html`).on('change', browsersync.reload);

});

gulp.task('prd.serve', ['prd.build'], () => {

  browsersync.init({
    server: `./${config.paths.dist}`
  });

  gulp.watch(`${config.paths.src}/**/**.js`, ['prd.build']);
  gulp.watch(`${config.paths.src}/**/**.css`, ['prd.build']);
  gulp.watch(`${config.paths.src}/**/**.tag`, ['prd.build']);
  gulp.watch(`${config.paths.src}/**/**.html`, ['prd.build']);

  gulp.watch(`${config.paths.dist}/**/**.js`).on('change', browsersync.reload);
  gulp.watch(`${config.paths.dist}/**/**.tag`).on('change', browsersync.reload);
  gulp.watch(`${config.paths.dist}/**/**.html`).on('change', browsersync.reload);

});
