var gulp         = require('gulp');
var config       = require('../config.json');
var usemin       = require('gulp-usemin');
var uglify       = require('gulp-uglify');
var minifyhtml   = require('gulp-htmlmin');
var minifycss    = require('gulp-clean-css');
var rev          = require('gulp-rev');

gulp.task('dev.minify', () => {
  return gulp.src(`./${config.paths.src}*.html`)
    .pipe(usemin({
      css: [minifycss(options.dev.minifycss)]
    }))
    .pipe(gulp.dest(`./${config.paths.dist}`));
});

gulp.task('dev.minify.html', () => {
  return gulp.src(`./${config.paths.src}${config.sources.html}`)
    .pipe(minifyhtml(options.dev.minifyhtml))
    .pipe(gulp.dest(`./${config.paths.dist}`));
});

gulp.task('dev.minify.tag', () => {
  return gulp.src(`./${config.paths.src}${config.sources.tag}`)
    .pipe(minifyhtml(options.dev.minifytag))
    .pipe(gulp.dest(`./${config.paths.dist}`));
});

var options = options || {};
options.dev = options.dev || {};

options.dev.minifycss = {
  relativeTo: `${config.paths.images}`
};

options.dev.minifyhtml = {
  removeComments: true,
  removeScriptTypeAttributes: true,
  minifyJS: true,
  minifyCSS: true
};

options.dev.minifytag = {
  removeComments: true,
  preserveLineBreaks: true,
  caseSensitive: true
};
