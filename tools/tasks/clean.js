var gulp   = require('gulp');
var config = require('../config.json');
var del    = require('del');

gulp.task('clean', () => {

  return del([`./${config.paths.dist}`]);

});
