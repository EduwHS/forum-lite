var gulp   = require('gulp');
var config = require('../config.json');

gulp.task('copy.images', () => {
  return gulp.src(config.sources.images)
    .pipe(gulp.dest(`./${config.paths.dist}${config.paths.images}`));
});
