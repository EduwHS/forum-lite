((scope) => {
  'use strict';

  class Service {
    constructor () {
      log.debug(`initializing service ${config.api.name}`);
      this.get = new Firebase(`https://${config.api.name}.firebaseio.com/`);
      log.debug(`done initializing service ${config.api.name}`);
    }
  }

  scope.Service = Service;

})(typeof window !== 'undefined' ? window : this);
