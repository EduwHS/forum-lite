((scope) => {
  'use strict';

  class Configuration {
    constructor () {
      this.api = api;
      this.classes = classes;
      this.validator = validator;
      this.pagination = pagination;
      this.debug = util.getParameter('debug') === 'true';
    }
  }

  let api = {};
  api.name = 'forum-lite';
  api.categories = 'categories';
  api.topics = 'topics';

  let pagination = {};
  pagination.records = 10;

  let classes = {};
  classes.error = 'error';

  let validator = {};
  validator.inputs = ['INPUT', 'TEXTAREA'];

  scope.Configuration = Configuration;

})(typeof window !== 'undefined' ? window : this);
