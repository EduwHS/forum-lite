((scope, $) => {
  'use strict';

  class Util {
    constructor () {
      this.validate = validate;
      this.validator = validator;
      this.getParameter = getParameter;
    }
  }

  let validator = {};
  validator.clear = validatorClear;

  function getParameter(name) {
    let regex = new RegExp(`.*[?&]${name}=([^&]+)(&|$)`);
    let match = window.location.href.match(regex);
    return (match ? match[1] : false);
  }

  function validate(element) {
    let tag = element.tagName;
    (tag === 'FORM') ? validateForm(element) : validateInput(element);
    return document.getElementsByClassName(config.classes.error).length ? false : true;
  }

  function validateForm(form) {
    _.forEach(form, (element) => validateInput(element));
  }

  function validateInput(input) {
    let tag = input.tagName;
    if (!_.includes(config.validator.inputs, tag)) return;

    log.debug('validating', input);
    input.checkValidity()
      ? $(input).closest('.field').removeClass(config.classes.error)
      : $(input).closest('.field').addClass(config.classes.error);
  }

  function validatorClear(form) {
    $(form).find('.field').removeClass(config.classes.error);
  }

  scope.Util = Util;

})(typeof window !== 'undefined' ? window : this, jQuery);
