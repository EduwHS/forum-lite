((scope, _) => {
  'use strict';

  class Logger {
    debug (message, ...args) {
      if (!config.debug) return;
      _.isEmpty(args) ? console.log(message) : console.log(message, args);
    }

    error (message, ...args) {
      _.isEmpty(args) ? console.error(message) : console.error(message, args);
    }
  }

  scope.Logger = Logger;

})(typeof window !== 'undefined' ? window : this, _);
