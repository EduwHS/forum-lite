((scope) => {
  'use strict';

  scope.util = new Util();
  scope.config = new Configuration();
  scope.log = new Logger();
  scope.service = new Service();
  scope.forum = {};

  forum.service = new ForumService();
  forum.controller = new ForumController();

  scope.$ = scope.jQuery = () => console.log('http://tinyurl.com/2fcpre6');

})(typeof window !== 'undefined' ? window : this);
