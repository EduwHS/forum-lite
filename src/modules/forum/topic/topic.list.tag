<topic-list show="{ topics }">
  <style scoped>
    .title p { display: inline-block; }
    .title p::first-letter { text-transform: capitalize; }
    .topics { margin-top: 3rem !important; }
    .content { margin-left: 1rem !important; }
  </style>

  <div class="ui fluid divider topics"></div>

  <div hide="{ empty }" class="ui styled fluid accordion">
    <div each="{ topics }" class="topic">
      <div class="title">
        <i class="dropdown icon"></i>
        <p>{ title }</p>
      </div>
      <div class="content">
        <div class="transition">
          <p class="content">{ content }</p>
        </div>
      </div>
    </div>
  </div>

  <div show="{ empty }" class="ui fluid warning message">
    <div class="title">
      No topics in this category yet.
    </div>
  </div>

  <div class="ui divider infinite-scroll"></div>

  <button show="{ loading && more }" class="medium fluid ui primary loading button disabled">Loading</button>

  <button show="{ more }" hide="{ loading || empty }" onclick="{ load }" class="medium fluid ui primary button">
    <i class="angle double down icon"></i>
  </button>

  <button hide="{ more }" class="medium fluid ui disabled button">
    <i class="angle double down icon"></i>
  </button>

  <script>
  var self = this;
  self.category = opts.category;
  self.topics = opts.topics;

  self.more = true;
  self.empty = false;
  self.loading = false;

  self.on('update', () => self.empty = _.isEmpty(self.topics));
  self.on('updated', () => self.init());

  init() {
    if (self.empty) return;
    forum.controller.topicBindings(self.load);
  }

  load() {
    if (self.loading) return;
    self.loading = true;

    var last = _.last(self.topics);
    forum.service.get.topics(self.category.id, last.updatedDate)
      .then((topics) => {
        if (_.isEmpty(topics)) {
          self.more = false;
          self.update();
          return;
        }
        self.topics = _.concat(self.topics, topics);
        self.loading = false;
        self.update();
      });
  }
  </script>
</topic-list>
