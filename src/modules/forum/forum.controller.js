((scope, $) => {
  'use strict';

  class ForumController {
    constructor () {
      forum.service.get.categories().then((categories) => {
        riot.mount('forum', {
          categories: categories
        });
      });
    }

    forumBindings () {
      $('forum .menu .item').on('click',
        (event) => $(event.currentTarget).addClass('active').siblings().removeClass('active'));
    }

    topicBindings (callback) {
      $('.infinite-scroll').visibility({
        once: false,
        initialCheck: false,
        observeChanges: true,
        onBottomVisible: () => { if(callback) callback(); }
      });

      $('.ui.accordion').accordion();
    }
  }

  scope.ForumController = ForumController;

})(typeof window !== 'undefined' ? window : this, jQuery);
