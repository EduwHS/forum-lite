<category show="{ category.id }">
  <topic-form></topic-form>

  <script>
    var self = this;
    self.category = opts.category;
    self.topics = opts.topics;

    self.on('mount', () => self.init());
    self.on('update', () => self.init());

    init() {
      self.tags['topic-form'].category = self.category;
      self.getTopics();
    }

    getTopics() {
      if(typeof self.category === 'undefined') return;

      log.debug('loading topics...');
      forum.service.get.topics(self.category.id)
        .then((topics) => {
          log.debug('topics loaded', topics);
          riot.mount('topic-list', {
            topics: topics,
            category: self.category
          });
      });
    }
  </script>
</category>
