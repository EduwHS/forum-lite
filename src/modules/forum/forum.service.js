((scope, _) => {
  'use strict';

    class ForumService {
      constructor () {
        this.get = getters;
        this.topic = topic;
      }
    }

    let getters = {};
    getters.categories = getCategories;
    getters.topics = getTopics;

    let topic = {};
    topic.create = createTopic;

    scope.ForumService = ForumService;

    function getCategories() {
      return getSorted(config.api.categories, 'priority');
    }

    function getTopics(category, startingAt) {
      let id = category;
      if (typeof id === 'object') id = category.id;

      return getPaginated(`${config.api.topics}/${id}`, startingAt, 'updatedDate');
    }

    /**
     * @param  {String} path
     *                    the service path to retrieve the records
     */
     function get(path) {
       log.debug(`get ${path}`);

       return new Promise((resolve, reject) => {
         service.get.child(path).once('value', (snapshot) => resolve(snapshot.val()));
       });
     }

     /**
      * @param  {String} path
      *                    the service path to retrieve the sorted records
      * @param  {String} sortBy
      *                    the property used to sort the values
      */
     function getSorted(path, sortBy) {
       log.debug(`getSorted sorting by ${sortBy}`);

       return new Promise((resolve, reject) => {
         get(path).then((values) => resolve(_.sortBy(values, sortBy)));
       });
     }

    /**
     * @param  {String} path
     *                    the service path to retrieve the records
     * @param  {Number} start
     *                    the value on which the page will start
     * @param  {String} key
     *                    the record property that the start value is refering to
     */
    function getPaginated(path, start, key) {
      if (!key) throw new PaginateWithoutKeyException();

      let startAt = start || 0;
      let limit = config.pagination.records;

      log.debug(`paginate '${path}' starting at '${startAt}' by key '${key}' limited to '${limit}'`);

      return new Promise((resolve, reject) => {
        service.get.child(path)
          .orderByChild(key)
          .startAt(startAt, key)
          .limitToFirst(limit)
          .once('value', (snapshot) => resolve(_.sortBy(snapshot.val(), key)));
      });

      function PaginateWithoutKeyException() {
          log.error('An object key property is required in order to paginate the records');
      }
    }

    /**
     * @param  {Object} category
     *                      category object or category id
     * @param  {Object} topic
     *                      topic to be created
     */
    function createTopic(category, topic) {
      if (typeof category === 'undefined') throw new CreateTopicWithoutCategoryException();

      let id = category;
      if (typeof id === 'object') id = category.id;

      let now = _.now();
      topic.createdDate = now;
      topic.updatedDate = now;

      log.debug(`creating topic %o in category ${id}`, topic);
      service.get.child(`${config.api.topics}/${id}`).push(topic);

      function CreateTopicWithoutCategoryException() {
        log.error('A category id is required to create a topic');
      }
    }

})(typeof window !== 'undefined' ? window : this, _);
