<forum>
  <style scoped>
    .menu-container { margin-top: 0.5rem; }
    .menu p { padding: 0.45rem 5rem; }
    .menu p::first-letter { text-transform: capitalize; }
  </style>

  <button show="{ visible }" onclick="{ toggle }" class="fluid ui red basic button">
    <i class="hide icon"></i>
  </button>
  <button hide="{ visible }" onclick="{ toggle }" class="fluid ui green basic button">
    <i class="unhide icon"></i>
  </button>

  <div class="menu-container" show="{ visible }">
    <div class="ui fluid vertical menu">
        <a each="{ categories }" onclick="{ select }" class="item">
          <p>{ title }</p>
        </a>
    </div>

    <category></category>
  </div>

  <script>
    var self = this;
    self.categories = opts.categories;
    self.categories.selected = {};
    self.visible = true;

    log.debug('forum initialized with categories', self.categories);

    self.on('mount', () => forum.controller.forumBindings());
    self.on('updated', () => { if (self.categories.changed) forum.controller.forumBindings(); });

    select(e) {
      self.categories.changed = self.categories.selected.id !== e.item.id;
      if (self.categories.changed) self.refresh(e);
    }

    refresh(e) {
      self.categories.selected = e.item;
      self.tags['category'].category = self.categories.selected;
      self.update('category');
    }

    toggle() {
      self.visible = !self.visible;
    }
  </script>
</forum>
