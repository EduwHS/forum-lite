<topic-form>
  <style scoped>
    form { margin-top: 1rem; }
    textarea { resize: none!important; }
    i.chevron { border: 0.095rem solid; }
  </style>

  <button hide="{ visible }" onclick="{ toggle }" class="fluid ui green basic button">
    <i class="comments icon"></i>Create new topic
  </button>
  <button show="{ visible }" onclick="{ toggle }" class="fluid ui red basic button">
    <i class="remove icon"></i>Cancel
  </button>

  <form show="{ visible }" class="ui form" id="topic_form">
    <div class="field">
      <input type="text" onblur="{ validate }" placeholder="Title..." id="topic_form_title" required minlength="10">
    </div>
    <div class="field">
      <div class="ui icon input">
        <textarea onblur="{ validate }" id="topic_form_content" required minlength="15"></textarea>
        <i onclick="{ create }" class="chevron right link green icon"></i>
      </div>
    </div>

    <div class="field">
      <button onclick="{ reset }" class="ui red basic icon button">
        <i class="trash outline icon"></i>
      </button>
    </div>
  </form>

  <script>
    var self = this;
    self.category = opts.category;
    self.visible = false;

    toggle() {
      self.visible = !self.visible;
    }

    reset() {
      topic_form.reset();
      util.validator.clear(topic_form);
    }

    validate(e) {
      util.validate(e.target);
    }

    create() {
      if (!util.validate(topic_form)) return;

      var topic = {};
      topic.title = topic_form_title.value;
      topic.content = topic_form_content.value;

      forum.service.topic.create(self.category, topic);

      self.toggle();
      self.reset();
      self.parent.getTopics();
    }
  </script>
</topic-form>