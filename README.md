### TODO
    reply to topic
    social login
    
### Browser compatibility
| Chrome  | Firefox | Edge   |
|:--------|:--------|:-------|
| 49      |45       |13      |


### Dependencies

| Dependency                                | Installation                                 |
|:------------------------------------------|:---------------------------------------------|
| [Node.js](https://nodejs.org/en/)         | [download](https://nodejs.org/en/download/)  |
| [Gulp](http://gulpjs.com/)                | `npm install gulp -g`                        |

### Installation
    $ git clone https://github.com/EduwHS/forum-lite.git forum-lite
    $ cd forum-lite
    $ npm install

### Running
    $ gulp serve
